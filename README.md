# Checkfer Coding Test

This project uses Parcel.js to bundle the application and handle the local web server.

I used Parcel.js because it is lightweight and requires zero-config, making it perfect for a single page project like this.

## Getting Started

### Install dependencies

Navigate to the project root in Terminal and run

```
npm install
```

### Run locally

Bundle the application and launch the development server on http://localhost:1234

```
npm run dev
```

### Build for production

Create a production ready build in the `/dist/` directory.

```
npm build
```

## Notes

While I've tried to get the design as pixel-perfect as possible, there are a number of places where I've made sensible adjustments to margins, padding, font sizes etc, where values in the design file were not consistent.

The instructions said to complete at least two of the three main sections. I've created the markup for all sections but only fully completed the Community and Menu sections.

## Things I would do differently

Had this been a real project with a longer deadline I would have approached the project with the following changes:

- Built the project using React to break the markup down into components and stored the data for Menu and Recipes section in a JSON file/Database.
- Collaborated with the Designer to address some of the positioning desicions and to work out better animation timings.
- Further broke down my CSS into variables and mixins for more consistency.

---

## Exercise Instructions for reference

### 0. Navigation

- [x] 0.1 The top menu should be always visible.
- [x] 0.2 The left hand side component shows where we are inside the home page. The current subsection has a bigger band and shows the number on the middle.

## 1. Meet our community section

- [x] 1.1 The content appears in different rows slightly slower than the page scroll.

## 2. Location section

- [x] 2.1 The map is loaded from https://snazzymaps.com/style/116734/sg-test-exercise

## 3. Our menu section

- [x] 3.1 The content is divided in 4 column. The 1st and 3rd column move slower than the scroll speed with a parallax effect. (These columns move up). The 2nd and 4th column move with a negative parallax effect (These columns move down)

## 4. Popular recipes

- [x] 4.1 This section should scroll horizontally to see more content.

Developer link:
https://xd.adobe.com/spec/b0192e90-6c9b-47c6-6918-b0b1c86ef48c-c628/

Review link:
https://xd.adobe.com/view/f577089b-b1cb-47d8-78bd-520e719114c1-8346/?hints=off

Please select at least 2 of the sections: Meet our Community, Our menu, Popular Recipes. It is not mandatory to create all the sections. This exercise is looking to test your understanding of web principles, attention to detail and aesthetic sense.

If you have any question, please feel free to contact maria@studiographene.com
Thanks a lot for your work, looking forward to seeing it, and good luck!
 

