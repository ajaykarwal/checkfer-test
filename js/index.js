import AOS from "aos";
import "aos/dist/aos.css";

var checkfer = {
  init: function() {
    this.smoothNavAnchors();
    this.setupScrollSnapping();
    AOS.init();
    this.setMenuScrollClasses("our-menu");
  },

  smoothNavAnchors: function() {
    const navItems = document.querySelectorAll("nav.primary a");
    navItems.forEach(function(el) {
      el.onclick = function(event) {
        event.preventDefault();
        document
          .querySelector(this.getAttribute("href"))
          .scrollIntoView({ behavior: "smooth", block: "start" });
      };
    });
  },

  setupScrollSnapping: function() {
    const indicatorsContainer = document.querySelector(".indicators");
    const sections = document.querySelectorAll("main > .section");
    let currentIndex = 0;
    let sectionIndices = {};

    function renderIndicator() {
      indicatorsContainer.innerHTML = "";

      for (let i = 0; i < sections.length; i++) {
        let zeroPaddedSectionNumber = ("0" + (i + 1)).slice(-2);
        let indicator = document.createElement("div");

        indicator.classList.add("indicator");
        indicator.innerHTML =
          i === currentIndex
            ? `<span class='step current'>${zeroPaddedSectionNumber}</span>`
            : `<span class='step'></span>`;
        (function(i) {
          indicator.onclick = function() {
            sections[i].scrollIntoView({ behavior: "smooth", block: "start" });
          };
        })(i);

        indicatorsContainer.appendChild(indicator);
      }
    }

    let options = {
      root: null,
      threshold: 0.5
    };

    let observer = new IntersectionObserver((entries, observer) => {
      let activated = entries.reduce((max, entry) => {
        return entry.intersectionRatio > max.intersectionRatio ? entry : max;
      });

      if (activated.intersectionRatio > 0) {
        currentIndex = sectionIndices[activated.target.getAttribute("id")];
        renderIndicator();
      }
    }, options);

    for (let i = 0; i < sections.length; i++) {
      sectionIndices[sections[i].getAttribute("id")] = i;
      observer.observe(sections[i]);
    }
  },

  setMenuScrollClasses: function(elementID) {
    var el = document.getElementById(elementID);
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    var before = elemTop > 0;
    var scrollingIn = elemTop > 0 && elemTop < window.innerHeight - 1;
    var scrollingOut = elemTop < -1 && elemTop < window.innerHeight - 1;
    var fullyIn = elemTop < 1 && elemTop > -1;
    var after = elemBottom < 0 && !fullyIn;

    if (before && !scrollingIn) {
      el.classList.remove("scrolling-complete");
      el.classList.add("scrolling-top");
    }
    if (scrollingIn) {
      el.classList.remove("scrolling-top");
      el.classList.remove("scrolling-complete");
    }

    if (scrollingOut) {
      el.classList.remove("scrolling-complete");
      el.classList.remove("scrolling-bottom");
    }
    if (after) {
      el.classList.remove("scrolling-complete");
      el.classList.add("scrolling-bottom");
    }
  }
};

checkfer.init();

window.onscroll = () => {
  checkfer.setMenuScrollClasses("our-menu");
};
